<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/contacts', function (Request $request) {
    $results = DB::select('select * from contacts');
    return response()->json($results, 200);
});

Route::get('/contacts/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (carNotExists($id)) {
        abort(404);
    }

    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from contacts where id=:id', [
        'id' => $id,
    ]);

    // Así se devuelve un JSON.
    // El primer parámetro es el dato que se manda como JSON.
    // El segundo parámetro es el status code
    return response()->json($results[0], 200);
});

Route::post('/contacts', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into contacts (id, firstName, lastName, email, phone)
        values (:id, :firstName, :lastName, :email, :phone)
    ",
        $data
    );

    $results = DB::select('select * from contacts where id = :id', [
        'id' => $data['id'],
    ]);
    return response()->json($results[0], 200);
});

Route::get('/contacts/search/{inputDato}', function($inputDato) {
    //LIKE sql 
    $results = DB::select('select * from contacts where firstName LIKE "%' . $inputDato .cd '%"');
    return response()->json($results, 200);
});

Route::put('/contacts/{id}', function ($id) {
    if (carNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from contacts where id = :id",
        ['id' => $id]
    );

    DB::insert(
        "
        insert into contacts (id, firstName, lastName, email, phone)
        values (:id, :firstName, :lastName, :email, :phone)
    ",
        $data
    );

    $results = DB::select('select * from contacts where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::patch('/contacts/{id}', function ($id) {
    if (carNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update contacts SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );

    $results = DB::select('select * from contacts where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::delete('/contacts/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (carNotExists($id)) {
        abort(404);
    }

    DB::delete('delete from contacts where id = :id', ['id' => $id]);

    return response()->json('', 200);
});

// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('carNotExists')) {
    function carNotExists($id)
    {
        $results = DB::select('select * from contacts where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}
