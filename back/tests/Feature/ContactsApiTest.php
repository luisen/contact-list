<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class ContactsApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  contacts (
                id	TEXT NOT NULL,
                firstName	TEXT,
                lastName	TEXT,
                phone	TEXT,
                email	TEXT
            );
            INSERT INTO contacts VALUES ('1','Luisen', 'Poy','666666666','l@gmail.com');
            INSERT INTO contacts VALUES ('2','Andoni', 'Luna','777777777','a@gmail.com');
        ");
        // $this->withoutExceptionHandling();
    }

    public function testGetContacts()
    {
        $this->json('GET', 'api/contacts')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id' => '1',
                    'firstName' => 'Luisen',
                    'lastName' => 'Poy',
                    'phone' => '666666666',
                    'email' => 'l@gmail.com',
                ],
                [
                    'id' => '2',
                    'firstName' => 'Andoni',
                    'lastName' => 'Luna',
                    'phone' => '777777777',
                    'email' => 'a@gmail.com',
                ],
            ]);
    }

    public function testGetContact()
    {
        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'id' => '1',
                    'firstName' => 'Luisen',
                    'lastName' => 'Poy',
                    'phone' => '666666666',
                    'email' => 'l@gmail.com',
                ]
            );
    }

    public function testPostContact()
    {
        $this->json('POST', 'api/contacts', [
            'id' => '99',
            'firstName' => 'Moha',
            'lastName' => 'Saadi',
            'phone' => '888888888',
            'email' => 'm@gmail.com',
        ])->assertStatus(200);

        $this->json('GET', 'api/contacts/99')
            ->assertStatus(200)
            ->assertJson([
                'id' => '99',
                'firstName' => 'Moha',
                'lastName' => 'Saadi',
                'phone' => '888888888',
                'email' => 'm@gmail.com',
            ]);
    }

    public function testPut()
    {
        $data = [
            'id' => '1',
            'firstName' => 'Luisen',
            'lastName' => 'Poy',
            'phone' => '999999999',
            'email' => 'l@gmail.com',
        ];

        $expected = [
            'id' => '1',
            'firstName' => 'Luisen',
            'lastName' => 'Poy',
            'phone' => '999999999',
            'email' => 'l@gmail.com',
        ];

        $this->json('PUT', 'api/contacts/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

    public function testPatch()
    {
        $this->json('PATCH', 'api/contacts/1', [
            'phone' => '555555555',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'firstName' => 'Luisen',
                'lastName' => 'Poy',
                'phone' => '555555555',
                'email' => 'l@gmail.com',
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'phone' => '555555555',
            ]);

        $this->json('PATCH', 'api/contacts/1', [
            'firstName' => 'Luisenri',
            'lastName' => 'Ktt',
            'email' => 'l@gmail.com',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'firstName' => 'Luisenri',
                'lastName' => 'Ktt',
                'phone' => '555555555',
                'email' => 'l@gmail.com',
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'firstName' => 'Luisenri',
                'lastName' => 'Ktt',
                'phone' => '555555555',
                'email' => 'l@gmail.com',
            ]);
    }


    public function testDeleteContact()
    {
        $this->json('GET', 'api/contacts/1')->assertStatus(200);

        $this->json('DELETE', 'api/contacts/1')->assertStatus(200);

        $this->json('GET', 'api/contacts/1')->assertStatus(404);
    }

    public function testGetContactNotExist()
    {
        // $this->json('GET', 'api/contacts/22')->assertStatus(404);

        $this->json('DELETE', 'api/contacts/22')->assertStatus(404);

        $this->json(
            'PUT',
            'api/contacts/22',
            [
                'id' => '1',
                'firstName' => 'Luisenri',
                'lastName' => 'Ktt',
                'phone' => '555555555',
                'email' => 'l@gmail.com',
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/contacts/22', [
            'phone' => '555555555',
        ])
            ->assertStatus(404);
    }
}
