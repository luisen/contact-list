import { shallowMount } from "@vue/test-utils";
import SignUp from "@/components/SignUp.vue";

test("Este el Button de anadir ", async () => {
  const user2 = {
    username: "mohammed",
    email: "mohamed@gmail.com",
    password: "123",
    validaRepetirPassword: "123"
  };
  const wrapper = shallowMount(SignUp, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.username = user2.username;
  wrapper.vm.email = user2.email;
  wrapper.vm.password = user2.password;
  wrapper.vm.validaRepetirPassword = user2.validaRepetirPassword;

  const button = wrapper.find(".btnAccept");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "accept",
      args: ["mohammed", "mohamed@gmail.com", "123", "123"]
    }
  ]);
});
