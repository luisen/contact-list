import { shallowMount } from "@vue/test-utils";
import ReserveAppointment from "@/components/ReserveAppointment.vue";

test("Este el Button de anadir ", async () => {
  const user1 = {
    date: "12",
    description: "cortar pello",
    starttime: "12:30",
    endtime: "1:00"
  };
  const wrapper = shallowMount(ReserveAppointment, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.date = user1.date;
  wrapper.vm.description = user1.description;
  wrapper.vm.starttime = user1.starttime;
  wrapper.vm.endtime = user1.endtime;

  const button = wrapper.find(".butonregistr");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "confermappointment",
      args: ["12", "cortar pello", "12:30", "1:00"]
    }
  ]);
});

test("pinta la tabla de user1", () => {
    const wrapper = shallowMount(ReserveAppointment, {
      propsData: {
        user1: {
          date: null,
    description: null,
    starttime: null,
    endtime: null
        }
      }
    });
    expect(wrapper.text()).toContain("");
    expect(wrapper.text()).toContain("");
    expect(wrapper.text()).toContain("");
    expect(wrapper.text()).toContain("");
    expect(wrapper.text()).toContain("");
  });