import { shallowMount } from "@vue/test-utils";
import Login from "@/components/Login.vue";

test("Este el Button de anadir ", async () => {
  const user2 = {
    email: "Admin",
    password: "Admin"
  };
  const wrapper = shallowMount(Login, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.email = user2.email;
  wrapper.vm.password = user2.password;

  const button = wrapper.find(".btn");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "emitvalidemail",
      args: ["Admin", "Admin"]
    }
  ]);
});
