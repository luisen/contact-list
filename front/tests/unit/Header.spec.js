import { shallowMount } from "@vue/test-utils";
import Header from "@/components/Header.vue";

test("Este el Button de ", async () => {
  const wrapper = shallowMount(Header);
  expect(wrapper.emitted().administrator).toBe(undefined);

  const button = wrapper.find(".Take");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().login.length).toBe(1);
  expect(wrapper.emitted().login[0]).toEqual([]);
});
